package com.mystore.testcases;

import com.mystore.base.Baseclass;
import com.mystore.pageobjects.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Addtocarttest extends Baseclass {
    Addtocart cart;
    Loginpage loginpage;
    Index indexpage;
    Search search;
    details detail;
    @BeforeMethod
    public void setup(){
        applaunch();
        indexpage = new Index();
        loginpage = new Loginpage();
        search = new Search();
        detail=new details();
        cart=new Addtocart();
    }
    @Test(groups = "Sanity")
    public void verifyAddtoCart() throws InterruptedException {
        indexpage.clickOnSignIn();
        indexpage.logoDisplay();
        loginpage.loginByEmail();
        search.searchProducts("shoes");
        detail.getDetails();
        detail.switchToNewTab();
        cart.clickCart();
    }
    @AfterMethod
    public void teardown(){
        driver.close();
    }
}
