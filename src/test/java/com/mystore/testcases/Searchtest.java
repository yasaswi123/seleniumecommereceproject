package com.mystore.testcases;

import com.mystore.base.Baseclass;
import com.mystore.pageobjects.Index;
import com.mystore.pageobjects.Loginpage;
import com.mystore.pageobjects.Search;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.mystore.base.Baseclass.applaunch;

public class Searchtest extends Baseclass {
    Loginpage loginpage;
    Index indexpage;
    Search search;
    @BeforeMethod
    public void setup(){
        applaunch();
        indexpage = new Index();
        loginpage = new Loginpage();
        search = new Search();
    }
    @Test(groups = "Sanity")
    public void verifySearch() throws InterruptedException {
        indexpage.clickOnSignIn();
        indexpage.logoDisplay();
        loginpage.loginByEmail();
        search.searchProducts("shoes");
    }
    @AfterMethod
    public void teardown(){
        driver.close();
    }
}
