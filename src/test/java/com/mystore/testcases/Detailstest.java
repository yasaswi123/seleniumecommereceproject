package com.mystore.testcases;

import com.mystore.base.Baseclass;
import com.mystore.pageobjects.Index;
import com.mystore.pageobjects.Loginpage;
import com.mystore.pageobjects.Search;
import com.mystore.pageobjects.details;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.mystore.base.Baseclass.applaunch;

public class Detailstest extends Baseclass {
    Loginpage loginpage;
    Index indexpage;
    Search search;
    details detail;
    @BeforeMethod
    public void setup(){
        applaunch();
        indexpage = new Index();
        loginpage = new Loginpage();
        search = new Search();
        detail=new details();
    }
    @Test(groups = "Sanity")
    public void verifyProductdetails() throws InterruptedException {
        indexpage.clickOnSignIn();
        indexpage.logoDisplay();
        loginpage.loginByEmail();
        search.searchProducts("shoes");
        detail.getDetails();
        detail.switchToNewTab();
    }
    @AfterMethod
    public void teardown(){
        driver.close();
    }
}
