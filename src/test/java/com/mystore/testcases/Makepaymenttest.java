package com.mystore.testcases;

import com.mystore.base.Baseclass;
import com.mystore.pageobjects.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Makepaymenttest extends Baseclass {
    Index indexpage;
    Loginpage loginpage;
    Search search;
    details detail;
    Checkout checkout;
    Addtocart cart;
    payment pay;
    Makepayment payment;
    @BeforeClass
    public void beforeClass(){
        applaunch();
        indexpage = new Index();
        loginpage = new Loginpage();
        search = new Search();
        detail=new details();
        cart=new Addtocart();
        checkout=new Checkout();
        pay=new payment();
        payment=new Makepayment();
    }
    @Test
    public void verifyMakePayment() throws InterruptedException {
        indexpage.clickOnSignIn();
        indexpage.logoDisplay();
        loginpage.loginByEmail();
        search.searchProducts("shoes");
        detail.getDetails();
        detail.switchToNewTab();
        cart.clickCart();
        checkout.validateCheckout();
        pay.paymentPage();
        payment.cardpayment();
    }
}
