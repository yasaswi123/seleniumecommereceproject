package com.mystore.testcases;

import com.mystore.base.Baseclass;
import com.mystore.pageobjects.Index;
import com.mystore.pageobjects.Loginpage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Logintest extends Baseclass {
    Index indexpage;
    Loginpage loginpage;
    @BeforeMethod
    public void setup(){
        loadConfig();
        applaunch();
        indexpage = new Index();
        loginpage = new Loginpage();
    }
    @Test(groups = "Smoke")
    public void verifylogin() throws Throwable{
        indexpage.clickOnSignIn();
        indexpage.logoDisplay();
       loginpage.loginByEmail();
    }
    @AfterMethod
    public void teardown(){
        driver.close();
    }
}
