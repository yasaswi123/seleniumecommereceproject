package com.mystore.testcases;

import com.mystore.base.Baseclass;
import com.mystore.pageobjects.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Checkouttest extends Baseclass {
    Index indexpage;
    Loginpage loginpage;
    Search search;
    details detail;
    Checkout checkout;
    Addtocart cart;
    @BeforeMethod
    public void beforeClass(){
        applaunch();
        indexpage = new Index();
        loginpage = new Loginpage();
        search = new Search();
        detail=new details();
        cart=new Addtocart();
        checkout=new Checkout();
    }
    @Test(groups = "Sanity")
    public void verifyCheckout() throws InterruptedException {
        indexpage.clickOnSignIn();
        indexpage.logoDisplay();
        loginpage.loginByEmail();
        search.searchProducts("shoes");
        detail.getDetails();
        detail.switchToNewTab();
        cart.clickCart();
        checkout.validateCheckout();
    }
    @AfterMethod
    public void teardown(){
        driver.close();
    }
}
