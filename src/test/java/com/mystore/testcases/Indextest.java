package com.mystore.testcases;

import com.mystore.base.Baseclass;
import com.mystore.pageobjects.Index;
import org.testng.annotations.*;

import static org.testng.AssertJUnit.assertTrue;

public class Indextest extends Baseclass {
    Index indexpage;
    @BeforeMethod
    public void setup(){
        loadConfig();
        applaunch();
        indexpage = new Index();
    }
    @Test(groups = "Smoke")
    public void verifylogin() {
        indexpage.clickOnSignIn();
    }
    @Test(groups = "Smoke")
    public void verifylogo() {
        Boolean logo=indexpage.logoDisplay();
        assertTrue(logo);
    }
    @Test(groups = "Smoke")
    public void verfiyTitle(){
        indexpage.getStoreTitle();
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }
}
