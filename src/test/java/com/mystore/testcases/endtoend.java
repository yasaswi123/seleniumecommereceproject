package com.mystore.testcases;

import org.testng.annotations.*;
import com.mystore.base.Baseclass;
import com.mystore.pageobjects.*;

public class endtoend extends Baseclass {
    Index indexpage;
    Loginpage login;
    Search search;
    Checkout checkout;
    Addtocart cart;
    details detail;
    payment pay;
    Makepayment card;
    @BeforeMethod
    public void setup(){
        applaunch();
        indexpage = new Index();
        login=new Loginpage();
        search=new Search();
        checkout=new Checkout();
        cart=new Addtocart();
        detail=new details();
        pay=new payment();
        card=new Makepayment();
    }

    @Test
    public void verifyAll() throws Throwable{
        indexpage.clickOnSignIn();
        indexpage.logoDisplay();
        indexpage.getStoreTitle();
        login.loginByEmail();
        search.searchProducts("shoes");
        detail.getDetails();
        detail.switchToNewTab();
        checkout.validateCheckout();
        pay.paymentPage();
        card.cardpayment();
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }

}
