package com.mystore.testcases;

import com.mystore.base.Baseclass;
import com.mystore.pageobjects.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Paymenttest extends Baseclass {
    Index indexpage;
    Loginpage loginpage;
    Search search;
    details detail;
    Checkout checkout;
    Addtocart cart;
    payment pay;
    @BeforeMethod
    public void beforeClass(){
        applaunch();
        indexpage = new Index();
        loginpage = new Loginpage();
        search = new Search();
        detail=new details();
        cart=new Addtocart();
        checkout=new Checkout();
        pay=new payment();
    }
    @Test
    public void verifyPayment() throws InterruptedException {
        indexpage.clickOnSignIn();
        indexpage.logoDisplay();
        loginpage.loginByEmail();
        search.searchProducts("shoes");
        detail.getDetails();
        detail.switchToNewTab();
        cart.clickCart();
        checkout.validateCheckout();
        pay.paymentPage();
    }
    @AfterMethod
    public void teardown(){
        driver.close();
    }
}
