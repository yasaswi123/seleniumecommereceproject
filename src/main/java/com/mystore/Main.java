package com.mystore;

import com.mystore.pageobjects.*;

import static com.mystore.base.Baseclass.applaunch;
import static com.mystore.base.Baseclass.loadConfig;

public class Main {
    public static void main(String[] args) throws Exception {
        loadConfig();
        applaunch();
        Index index=new Index();
        index.clickOnSignIn();
        index.logoDisplay();
        index.getStoreTitle();
        Register register=new Register();
        register.clickCreateAccount();
        Loginpage loginbyno=new Loginpage();
        loginbyno.loginByEmail();
        index.logoDisplay();
        System.out.println(index.getStoreTitle());
        Search search=new Search();
        search.searchProducts("shoes");
        details detail=new details();
        detail.getDetails();
        detail.switchToNewTab();
        Addtocart cart=new Addtocart();
        cart.clickCart();
        Checkout checkout=new Checkout();
        checkout.validateCheckout();
        payment pay=new payment();
        pay.paymentPage();
    }
}
