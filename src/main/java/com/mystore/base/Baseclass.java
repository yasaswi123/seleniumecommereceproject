package com.mystore.base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;


import java.io.*;
import java.time.Duration;
import java.util.*;

public class Baseclass {
    public static Properties prop;
    public static WebDriver driver;
    @BeforeTest()
    public static void loadConfig() {
        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+"/configuration/config.properties");
            prop.load(ip);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void applaunch(){
        String browser = prop.getProperty("browser");
        //System.out.println(browser);
        if (browser.contains("chrome")) {
            driver = new ChromeDriver();
        }
        else if (browser.contains("firefox")) {
            driver = new FirefoxDriver();
        }
        else if (browser.contains("ie")) {
            driver = new InternetExplorerDriver();
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(40));
        //driver.manage().deleteAllCookies();
        driver.get(prop.getProperty("url"));
    }

}
