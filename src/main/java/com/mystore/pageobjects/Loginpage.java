package com.mystore.pageobjects;

import com.mystore.base.Baseclass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Loginpage extends Baseclass {
    @FindBy(xpath = "//iframe[@id='loginIframe']")
    WebElement loginIframe;
    @FindBy(xpath = "//input[@id='userName']")
    WebElement phonenumber;
    @FindBy(xpath="//button[@id='checkUser']")
    WebElement clickcontinue;
    public Loginpage() {
        PageFactory.initElements(driver, this);
    }
    public void loginByEmail() throws InterruptedException {
        driver.switchTo().frame(loginIframe);
        phonenumber.sendKeys("6300628645");
        clickcontinue.click();
        Thread.sleep(20000);
        driver.switchTo().defaultContent();
    }
    public Register createNewAccount() throws Throwable {
        return new Register();
    }
    public Search search(){
        return new Search();
    }


}

