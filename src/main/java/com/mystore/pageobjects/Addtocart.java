package com.mystore.pageobjects;

import com.mystore.actiondriver.Action;
import com.mystore.base.Baseclass;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Addtocart extends Baseclass {
    @FindBy(xpath="//div[@id='add-cart-button-id']")
    WebElement cart;
    public Addtocart() {
        PageFactory.initElements(driver, this);
    }
    public void clickCart() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,400)", "");
        cart.click();
    }
    public Checkout getCheckout() {
        return new Checkout();
    }
}
