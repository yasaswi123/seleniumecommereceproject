package com.mystore.pageobjects;

import com.mystore.base.Baseclass;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Search extends Baseclass {
    public Search(){
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//input[@id='inputValEnter']") WebElement search;
    public void searchProducts(String searchText){
        search.sendKeys(searchText);
        Actions action = new Actions(driver);
        action.keyDown(Keys.ENTER).keyUp(Keys.ENTER).perform();
    }
    public details detailsProduct(){
        return new details();
    }
}
