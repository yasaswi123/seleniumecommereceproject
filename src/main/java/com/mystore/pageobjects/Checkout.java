package com.mystore.pageobjects;

import com.mystore.base.Baseclass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Checkout extends Baseclass {
    public Checkout() {
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath="//a[normalize-space()='Proceed To Checkout']")
    WebElement checkout;
    public  void validateCheckout(){
        checkout.click();
    }


}
