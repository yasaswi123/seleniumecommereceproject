package com.mystore.pageobjects;

import com.mystore.base.Baseclass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Index extends Baseclass {
    public Index() {
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath="//div[@class='accountInner']")
    WebElement account;
    @FindBy(xpath = "//a[normalize-space()='login']")
    WebElement login;
    @FindBy(xpath="//img[@class='notIeLogoHeader aspectRatioEqual sdHomepage cur-pointer']")
    WebElement logo;

    public void clickOnSignIn(){
        Actions action = new Actions(driver);
        action.moveToElement(account).perform();
        login.click();
    }
    public boolean logoDisplay(){
        boolean Logo=logo.isDisplayed();
        if(Logo){
           return true;
        }
        else{
            return false;
        }
    }
    public String getStoreTitle() {
        String title=driver.getTitle();
        return title;
    }
    public Loginpage login(){
        return new Loginpage();
    }




}