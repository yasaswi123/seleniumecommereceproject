package com.mystore.pageobjects;

import com.mystore.base.Baseclass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class payment extends Baseclass {
    public payment() {
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath="//button[@id='make-payment']")
    WebElement proceedtopay;
    public void paymentPage(){
        proceedtopay.click();
    }
}
