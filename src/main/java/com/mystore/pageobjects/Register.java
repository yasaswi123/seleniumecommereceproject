package com.mystore.pageobjects;

import com.mystore.actiondriver.Action;
import com.mystore.base.Baseclass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Register extends Baseclass {

    @FindBy(xpath = "//div[@class='accountInner']")
    WebElement signIn;
    @FindBy(xpath = "//iframe[@id='loginIframe']")
    WebElement loginIframe;
    @FindBy(xpath = "//span[@class='newUserRegister']")
    WebElement Register;
    @FindBy(xpath = "//input[@id='userName']")
    WebElement phonenumber;
    @FindBy(xpath="//button[@id='checkUser']")
    WebElement clickcontinue;
    @FindBy(xpath="//input[@id='j_username_new']")
    WebElement email;
    @FindBy(xpath="//input[@id='j_name']")
    WebElement Name;
    @FindBy(xpath="//input[@id='j_dob']")
    WebElement dateofbirth;
    @FindBy(xpath="//input[@id='j_password']")
    WebElement password;
    @FindBy(xpath="//button[@id='userSignup']")
    WebElement continues;

    public Register(){
        PageFactory.initElements(driver, this);
    }
    public void clickCreateAccount() {
        Actions action = new Actions(driver);
        action.moveToElement(signIn).perform();
        Register.click();
        driver.switchTo().frame(loginIframe);
        phonenumber.sendKeys("9865457886");
        clickcontinue.click();
        email.sendKeys("aaaccc@gmail.com");
        Name.sendKeys("preethi");
        dateofbirth.sendKeys("05/05/2000");
        password.sendKeys("snap123");
        continues.click();
    }








}
