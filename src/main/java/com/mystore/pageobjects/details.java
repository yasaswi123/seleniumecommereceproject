package com.mystore.pageobjects;

import com.mystore.actiondriver.Action;
import com.mystore.base.Baseclass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;

import static com.mystore.base.Baseclass.driver;

public class details extends Baseclass {
    public details() {
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath="//img[@title='Bucik Brown Outdoor Shoes']")
    WebElement shoedetails;
    @FindBy(xpath="//input[@placeholder='Enter your pincode']")
    WebElement pincode;
    @FindBy(xpath=" //button[normalize-space()='Check']")
    WebElement check;

    public void enterPincode() {
        pincode.sendKeys("560029");
        check.click();
    }
    public void getDetails(){
        shoedetails.click();
    }
    public void switchToNewTab() {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
    }
    public Addtocart toCart(){
        return new Addtocart();
    }

}
